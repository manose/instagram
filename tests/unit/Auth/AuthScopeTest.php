<?php
namespace Manose\Instagram\Test\Auth;

use Mockery as M;
use Manose\Instagram\Auth\AuthException;
use Manose\Instagram\Auth\AuthScope;

/**
 * Class AuthScopeTest
 *
 * @group Auth
 * @coversDefaultClass AuthScope
 */
class AuthScopeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @cover ::addScope
     * @dataProvider dataAddScopePositive
     */
    public function testAddingScopePositive($scope)
    {
        $scopeAuth = new AuthScope();
        $scopeAuth->addScope($scope);
        $scope = strtolower($scope);

        $this->assertTrue(in_array($scope, $scopeAuth->getScopeList()));
    }

    /**
     * @cover ::addScope
     * @dataProvider dataAddNotExistedScope
     */
    public function testWrongScopeMustNotBeAddedToList($scope)
    {
        $scopeAuth = new AuthScope();
        try {
            $scopeAuth->addScope($scope);
        } catch (AuthException $e) {
        }

        $scope = strtolower($scope);

        $this->assertTrue(!in_array($scope, $scopeAuth->getScopeList()));
    }

    /**
     * @cover ::addScope
     * @expectedException \Manose\Instagram\Auth\AuthException
     * @dataProvider dataAddNotExistedScope
     */
    public function testAddNotExistedScope($scope)
    {
        $scopeAuth = new AuthScope();
        $scopeAuth->addScope($scope);
    }

    /**
     * @cover ::removeScope
     * @dataProvider scopeListToDelete
     */
    public function testRemoveScope($scope)
    {
        $scopeAuth = new AuthScope();
        $scopeAuth->removeScope($scope);
        $scopeList = $scopeAuth->getScopeList();

        $this->assertTrue(!in_array($scope, $scopeList));
    }

    /**
     * @cover ::addScope
     * @cover ::getScopeList
     * @cover ::removeScope
     * @dataProvider dataAddScopePositive
     */
    public function testCorrectAddRemoveFlow($scope)
    {
        $scopeAuth = new AuthScope();
        $scopeAuth->addScope($scope);
        $scope = strtolower($scope);

        $this->assertTrue(in_array($scope, $scopeAuth->getScopeList()));

        $scopeAuth->removeScope($scope);
        $this->assertFalse(in_array($scope, $scopeAuth->getScopeList()));
    }

    public function scopeListToDelete()
    {
        return
            array_merge(
                $this->dataAddNotExistedScope(),
                $this->dataAddScopePositive()
            );
    }

    public function dataAddNotExistedScope()
    {
        return [
            [PHP_INT_MAX],
            [''],
            [null],
            ['notExist'],
            ['1basic'],
            ['basic1'],
        ];
    }

    public function dataAddScopePositive()
    {
        return [
            [AuthScope::SCOPE_BASIC],
            [ucfirst(AuthScope::SCOPE_BASIC)],
            [strtoupper(AuthScope::SCOPE_COMMENTS)],
        ];
    }

    public function tearDown()
    {
        M::close();
    }
}
