<?php
namespace Manose\Instagram\Test\Auth;

use Manose\Instagram\Auth\AuthException;
use Manose\Instagram\Auth\RequestToken;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use GuzzleHttp\ClientInterface;
use Mockery as M;

/**
 * Class RequestTokenTest
 *
 * @group Auth
 * @coversDefaultClass RequestToken
 */
class RequestTokenTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var StreamInterface
     */
    private $streamInRequest;
    /**
     * @var RequestToken
     */
    private $requestToken;

    public function setUp()
    {
        $httpClient =  M::mock(ClientInterface::class);
        $httpResponse = M::mock(ResponseInterface::class);
        $streamInterface = M::mock(StreamInterface::class);

        $httpResponse->shouldReceive('getBody')->andReturn($streamInterface);
        $httpClient->shouldReceive('request')->andReturn($httpResponse);

        $config = [
            'client_id' => '',
            'callback_url' => '',
            'secret' => '',
        ];

        $this->streamInRequest = $streamInterface;
        $this->requestToken = new RequestToken($config, $httpClient);
    }

    /**
     * @cover ::doTokenRequest
     * @dataProvider dataRequestTokenPositive
     */
    public function testGoodTokenResponse($jsonContent)
    {
        $requestToken = $this->requestToken;
        $this->streamInRequest->shouldReceive('getContents')->andReturn($jsonContent);
        $requestToken->doTokenRequest('instaCode');
        $this->assertEmpty($requestToken->getError());
    }

    /**
     * @cover ::doTokenRequest
     * @expectedException \Manose\Instagram\Auth\AuthException
     * @dataProvider dataRequestInvalidResponse
     */
    public function testInvalidInvalidResponse($jsonContent)
    {
        $requestToken = $this->requestToken;
        $this->streamInRequest->shouldReceive('getContents')->andReturn($jsonContent);
        $requestToken->doTokenRequest('instaCode');
    }

    /**
     * @cover ::doTokenRequest
     * @dataProvider dataRequestInvalidResponse
     */
    public function testErrorsAfterException($jsonContent)
    {
        $requestToken = $this->requestToken;
        $this->streamInRequest->shouldReceive('getContents')->andReturn($jsonContent);

        try {
            $requestToken->doTokenRequest('instaCode');
        } catch (\Exception $e) {
            self::assertInstanceOf(AuthException::class, $e);
        }

        $this->assertNotEmpty($requestToken->getError());
    }

    public function dataRequestInvalidResponse()
    {
        return [
            ['not json'],
            [
                json_encode([
                    'error_type' => 'OAuthException',
                    'error_message' => 'error message',
                    ]
                )
            ],
        ];
    }

    public function dataRequestTokenPositive()
    {
        $response = json_encode(
            [
                'access_token' => '',
                'user' => [
                    'username' => '',
                    'bio' => '',
                    'website' => '',
                    'profile_picture' => '',
                    'full_name' => '',
                    'id' => '',
                ]
            ]
        );

        return [
            [
                $response
            ],
            [
                json_encode('some line')
            ],
            [
                json_encode([])
            ],
        ];
    }

    public function tearDown()
    {
        M::close();
    }
}
