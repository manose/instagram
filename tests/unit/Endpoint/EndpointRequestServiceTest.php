<?php
namespace Manose\Instagram\Test\Endpoint;

use GuzzleHttp\ClientInterface;
use Manose\Instagram\Endpoint\EndpointRequestService;
use Manose\Instagram\Endpoint\Relationship\FollowedBy;
use Mockery as M;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class EndpointRequestServiceTest
 *
 * @group Endpoint
 * @coversDefaultClass EndpointRequestService
 * @package Manose\Instagram\Test\Endpoint
 */
class EndpointRequestServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @cover ::doEndpointRequest
     */
    public function testDoEndpointRequest()
    {
        $streamMock = M::mock(StreamInterface::class);
        $streamMock->shouldReceive('getContents')->andReturn(json_encode(['data' => ['data']]));

        $responseMock = M::mock(ResponseInterface::class);
        $responseMock->shouldReceive('getBody')->andReturn($streamMock);

        $httpClientMock = M::mock(ClientInterface::class);
        $httpClientMock->shouldReceive('request')->andReturn($responseMock);

        $request = new EndpointRequestService('token', $httpClientMock);

        $contentJson = $request->doEndpointRequest(new FollowedBy());
        $content = json_decode($contentJson, true);

        $this->assertNotEmpty($content['data']);
    }

    public function tearDown()
    {
        M::close();
    }
}
