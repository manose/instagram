<?php
namespace Manose\Instagram\Auth;

use GuzzleHttp\ClientInterface;

class RequestToken
{
    const AUTH_API_URL = 'https://api.instagram.com/oauth/access_token';
    /**
     *  Is currently the only supported value of 'grant type'
     */
    const GRANT_TYPE_AUTH_CODE = 'authorization_code';

    /**
     * @var string
     */
    private $clientId;
    /**
     * The same redirect URI which is used to get CODE on step 1
     * @var string
     */
    private $redirectUri;
    /**
     * Instagram token API url
     *
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $secret;
    /**
     * @var ClientInterface
     */
    private $httpClient;
    /**
     * @var string
     */
    private $grantType;
    /**
     * @var string
     */
    private $error;

    /**
     * @param array           $config
     * @param ClientInterface $httpClient
     */
    public function __construct(array $config, ClientInterface $httpClient)
    {
        $this->url = self::AUTH_API_URL;
        $this->grantType = self::GRANT_TYPE_AUTH_CODE;
        $this->clientId = $config['client_id'];
        $this->redirectUri = $config['callback_url'];
        $this->secret = $config['secret'];
        $this->httpClient = $httpClient;
    }

    /**
     * Does request to Instagram to get a token by code.
     * Can be run only once for the same $code.
     * If request succeed returns array with access token and user info:
     * [
     *    "access_token" => "string",
     *    "user" => [
     *                  "username"          => "string",
     *                  "bio"               => "string",
     *                  "website"           => "string",
     *                  "profile_picture"   => "string",
     *                  "full_name"         => "string",
     *                  "id"                => "integer"
     *    ]
     * ]
     *
     * @param string $code
     *
     * @throws AuthException
     *
     * @return array
     */
    public function doTokenRequest($code)
    {
        $this->error = null;

        $postParams =  [
            'form_params' => [
                'client_secret' => $this->secret,
                'client_id' => $this->clientId,
                'grant_type' => 'authorization_code',
                'redirect_uri' => $this->redirectUri,
                'code' => $code,
            ]
        ];

        $response = $this->httpClient->request('POST', $this->url, $postParams);

        $tokenContent = $response->getBody()->getContents();

        if (!$this->isValidTokenContent($tokenContent)) {
            throw new AuthException($this->error);
        }

        return json_decode($tokenContent, true);
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Check is JSON valid and is response has error message.
     *
     * @param $tokenContent
     *
     * @return bool
     */
    private function isValidTokenContent($tokenContent)
    {
        $isValid = true;
        $content = json_decode($tokenContent, true);

        if (json_last_error()) {
            $isValid = false;
            $this->error = 'JSON error:' . json_last_error_msg();
        } elseif (isset($content['error_type'])) {
            $isValid = false;
            $this->error = $content['error_message'];
        }

        return $isValid;
    }
}
