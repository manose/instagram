<?php
namespace Manose\Instagram\Auth;

/**
 * Class AuthScope
 * @package Manose\Instagram\Auth
 */
class AuthScope
{
    /**
     * Predefined scopes
     * @see https://www.instagram.com/developer/authorization/
     */
    const SCOPE_BASIC = 'basic';
    const SCOPE_COMMENTS = 'comments';
    const SCOPE_CONTENT = 'public_content';
    const SCOPE_FOLLOWER = 'follower_list';
    const SCOPE_RELATIONSHIPS = 'relationships';
    const SCOPE_LIKES = 'likes';

    const SCOPE_LIST = [
        self::SCOPE_BASIC,
        self::SCOPE_COMMENTS,
        self::SCOPE_CONTENT,
        self::SCOPE_FOLLOWER,
        self::SCOPE_RELATIONSHIPS,
        self::SCOPE_LIKES,
    ];
    /**
     * List of scopes which Instagram will be requested of.
     *
     * @var array
     */
    private $scopeUseList;

    public function __construct()
    {
        //Set default scope
        $this->scopeUseList[self::SCOPE_BASIC] = self::SCOPE_BASIC;
    }

    /**
     * Add scope permissions from user to have access to their data
     *
     * @param string $scope
     *
     * @return $this
     * @throws AuthException
     */
    public function addScope($scope)
    {
        $scope = strtolower($scope);

        if (!in_array($scope, self::SCOPE_LIST)) {
            throw new AuthException('Scope not in the list: ' . $scope);
        }

        $this->scopeUseList[$scope] = $scope;

        return $this;
    }

    /**
     * Remove scope from the list
     * 
     * @param string $scope
     *
     * @return $this
     */
    public function removeScope($scope)
    {
        unset($this->scopeUseList[$scope]);

        return $this;
    }

    /**
     * Get all defined scopes
     *
     * @return array
     */
    public function getScopeList()
    {
        return $this->scopeUseList;
    }
}
