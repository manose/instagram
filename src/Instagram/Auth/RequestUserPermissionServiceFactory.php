<?php
namespace Manose\Instagram\Auth;

class RequestUserPermissionServiceFactory
{
    /**
     * Create RequestUserPermission by provided config.
     * Config is array of:
     *  [
     *      'client_id'     => CLIENT_ID,
     *      'callback_url'  => URL_ON_CLIENT_SIDE,
     *      [optional]'state' => 'server-specific state like CSRF token',
     *  ]
     *
     * @param $config
     *
     * @return RequestUserPermission
     */
    public function create($config)
    {
        return new RequestUserPermission($config);
    }
}
