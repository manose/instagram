<?php
namespace Manose\Instagram\Auth;

use GuzzleHttp\Client;

class RequestTokenServiceFactory
{
    /**
     * Create RequestUserPermission by provided config.
     * Config is array of:
     *  [
     *      'client_id'     => CLIENT_ID,
     *      'secret'        => SECRET
     *      'callback_url'  => URL_ON_CLIENT_SIDE,
     *  ]
     *
     * @param $config
     *
     * @return RequestToken
     */
    public function create($config)
    {
        $client = new Client();

        return new RequestToken($config, $client);
    }
}
