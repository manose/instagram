<?php
namespace Manose\Instagram\Endpoint;

interface EndpointRequestServiceInterface
{
    /**
     * Do request to Instagram API.
     *
     * @param EndpointInterface $endpoint
     *
     * @return string
     */
    public function doEndpointRequest(EndpointInterface $endpoint);
}
