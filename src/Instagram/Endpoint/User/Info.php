<?php
namespace Manose\Instagram\Endpoint\User;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Info
 *
 * Get information about a user.
 * The public_content scope is required if the user is not the owner of the access_token.
 *
 * Required scope: public_content [if not self user]
 *
 * @package Manose\Instagram\Endpoint\User
 */
class Info extends AbstractEndpoint
{
    /**
     * Additional action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = 'users/{user-id}';

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     *
     * @param int $userId
     *
     * @return void
     */
    public function prepareParams($userId = null)
    {
        if (null === $userId) {
            $userId = self::MY_ACCOUNT_ALIAS;
        }

        $this->endpointAction = str_replace('{user-id}', $userId, self::ACTION_ENDPOINT);
    }
}
