<?php
namespace Manose\Instagram\Endpoint\User;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class MediaLiked
 *
 * Get the list of recent media liked by the owner of the access_token.
 *
 * Required scope: public_content
 *
 * @package Manose\Instagram\Endpoint\User
 */
class MediaLiked extends AbstractEndpoint
{
    /**
     * Additional action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = 'users/self/media/liked';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $count
     * @param int $maxId
     *
     * @return void
     */
    public function prepareParams($count = null, $maxId = null)
    {
        $params['count'] = $count;
        $params['max_like_id'] = $maxId;

        $this->endpointAction = self::ACTION_ENDPOINT;
        $this->endpointParams = $params;
    }
}
