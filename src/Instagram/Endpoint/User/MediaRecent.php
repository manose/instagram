<?php
namespace Manose\Instagram\Endpoint\User;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class MediaRecent
 *
 * Get the most recent media published by a user.
 * The public_content scope is required if the user is not the owner of the access_token.
 *
 * Required scope: public_content [if not self user]
 *
 * @package Manose\Instagram\Endpoint\User
 */
class MediaRecent extends AbstractEndpoint
{
    /**
     * Additional action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = 'users/{user-id}/media/recent/';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $userId
     * @param int $count
     * @param int $minId
     * @param int $maxId
     *
     * @return void
     */
    public function prepareParams($userId = null, $count = null, $minId = null, $maxId = null)
    {
        if (null === $userId) {
            $userId = self::MY_ACCOUNT_ALIAS;
        }

        $params['count'] = $count;
        $params['min_id'] = $minId;
        $params['max_id'] = $maxId;

        $this->endpointAction = str_replace('{user-id}', $userId, self::ACTION_ENDPOINT);
        $this->endpointParams = $params;
    }
}
