<?php
namespace Manose\Instagram\Endpoint\User;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Search
 *
 * Get a list of users matching the query.
 *
 * Required scope: public_content
 *
 * @package Manose\Instagram\Endpoint\User
 */
class Search extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = 'users/search';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param string $search
     * @param int    $count [optional]
     *
     * @return void
     */
    public function prepareParams($search, $count = null)
    {
        $params['count'] = $count;
        $params['q'] = $search;

        $this->endpointParams = $params;
        $this->endpointAction = self::ACTION_ENDPOINT;
    }
}
