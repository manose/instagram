<?php
namespace Manose\Instagram\Endpoint;

use Manose\Instagram\InstagramException;

class EndpointException extends InstagramException
{
}
