<?php
namespace Manose\Instagram\Endpoint\Location;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Info
 *
 * Get information about a location.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/locations/#get_locations
 * @package Manose\Instagram\Endpoint\Location
 */
class Info extends AbstractEndpoint
{
    /**
     * Action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = '/locations/{location-id}';

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $locationId
     *
     * @return void
     */
    public function prepareParams($locationId)
    {
        $this->endpointAction = str_replace('{location-id}', $locationId, self::ACTION_ENDPOINT);
    }
}
