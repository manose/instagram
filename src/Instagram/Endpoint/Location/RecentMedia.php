<?php
namespace Manose\Instagram\Endpoint\Location;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class RecentMedia
 *
 * Get a list of recent media objects from a given location.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/locations/#get_locations_media_recent
 * @package Manose\Instagram\Endpoint\Location
 */
class Search extends AbstractEndpoint
{
    /**
     * Action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = '/locations/{location-id}/media/recent';

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $locationId
     * @param $minId [optional]  Return media before this min_id.
     * @param $maxId [optional]  Return media after this max_id.
     *
     * @return void
     */
    public function prepareParams($locationId, $minId, $maxId)
    {
        $params['min_id'] = $minId;
        $params['max_id'] = $maxId;
        $this->endpointParams = $params;

        $this->endpointAction = str_replace('{location-id}', $locationId, self::ACTION_ENDPOINT);
    }
}
