<?php
namespace Manose\Instagram\Endpoint\Location;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Search
 *
 * Search for a location by geographic coordinates or facebook places ID.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/locations/#get_locations_search
 * @package Manose\Instagram\Endpoint\Location
 */
class Search extends AbstractEndpoint
{
    /**
     * Action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = '/locations/search';
    /**
     * Max search distance
     */
    const MAX_DISTANCE = 750;

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     * @param double $lat              Latitude of the center search coordinate. If used, lng is required.
     * @param double $lng              Longitude of the center search coordinate. If used, lat is required.
     * @param int    $facebookPlacesId [optional]  Returns a location mapped off of a Facebook places id. If used, lat and lng are not required.
     * @param int    $distance         [optional]  Default is 500m (distance=500), max distance is 750.
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($lat, $lng, $facebookPlacesId = null, $distance = null)
    {
        if ((null === $lat || null === $lng)
            && null === $facebookPlacesId
        ) {
            throw new EndpointException('Both Latitude and Longitude are required');
        }

        $params['lat'] = $lat;
        $params['lng'] = $lng;
        $params['facebook_places_id'] = $facebookPlacesId;
        $params['distance'] = $distance;

        $this->endpointParams = $params;
        $this->endpointAction = self::ACTION_ENDPOINT;
    }
}
