<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Like
 *
 * Get a list of users who have liked this media.
 *
 * Required scope: public_content public_content [required for media that does not belong to the owner of the access_token]
 *
 * @link https://www.instagram.com/developer/endpoints/likes/#get_media_likes
 * @package Manose\Instagram\Endpoint\Media
 */
class Like extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}/likes';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $mediaId
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId)
    {
        if (!is_int($mediaId)) {
            throw new EndpointException('Wrong Media ID: ' . $mediaId);
        }

        $this->endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
    }
}
