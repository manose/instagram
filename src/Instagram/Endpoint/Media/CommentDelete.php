<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class CommentDelete
 *
 * Remove a comment either on the authenticated user's media object or authored by the authenticated user.
 *
 * Required scope:  comments
 *                  public_content [required for media that does not belong to the owner of the access_token]
 *
 * @link https://www.instagram.com/developer/endpoints/comments/#post_media_comments
 * @link https://www.instagram.com/developer/endpoints/comments/#delete_media_comments
 *
 * @package Manose\Instagram\Endpoint\Media
 */
class CommentDelete extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}/comments';

    protected $httpMethod = self::HTTP_POST;

    /**
     * @param int $mediaId
     * @param int $commentId
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId, $commentId)
    {
        if (!is_numeric($mediaId)) {
            throw new EndpointException('Not valid Media ID');
        }

        if (!is_numeric($commentId)) {
            throw new EndpointException('Not valid Comment ID');
        }

        $endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
        $this->endpointAction = $endpointAction . '/' . $commentId;
    }
}
