<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Comment
 *
 * Get a list of recent comments on a media object.
 * The public_content scope is required for media that does not belong to the owner of the access_token.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/comments/#get_media_comments
 * @package Manose\Instagram\Endpoint\Media
 */
class Comment extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}/comments';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $mediaId
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId)
    {
        if (!is_int($mediaId)) {
            throw new EndpointException('Wrong Media ID: ' . $mediaId);
        }

        $this->endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
    }
}
