<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Info
 *
 * Get information about a media object.
 * Use the type field to differentiate between image and video media in the response.
 * You will also receive the user_has_liked field which tells you whether
 * the owner of the access_token has liked this media.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/media/#get_media
 * @package Manose\Instagram\Endpoint\Media
 */
class Info extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $mediaId
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId)
    {
        if (!is_int($mediaId)) {
            throw new EndpointException('Wrong Media ID: ' . $mediaId);
        }

        $this->endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
    }
}
