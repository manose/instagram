<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Search
 *
 * Search for recent media in a given area.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/media/#get_media_search
 * @package Manose\Instagram\Endpoint\Media
 */
class Search extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/search';
    /**
     * Max search distance
     */
    const MAX_DISTANCE = 5000;

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param double $lat [optional] Latitude of the center search coordinate. If used, lng is required.
     * @param double $lng [optional] Longitude of the center search coordinate. If used, lat is required.
     * @param int    $distance [optional] Default is 1km (distance=1000), max distance is 5km.
     *
     * @return void;
     *
     * @throws EndpointException
     */
    public function prepareParams($lat, $lng, $distance = null)
    {
        if (null === $lat || null === $lng) {
            throw new EndpointException('Latitude and Longitude cannot be null');
        }

        if ($distance > self::MAX_DISTANCE) {
            throw new EndpointException('Distance cannot be more than ' . self::MAX_DISTANCE . ' meters');
        }

        $params['lat'] = $lat;
        $params['lng'] = $lng;

        $params['distance'] = $distance;

        $this->endpointParams = $params;
        $this->endpointAction = self::ACTION_ENDPOINT;
    }
}
