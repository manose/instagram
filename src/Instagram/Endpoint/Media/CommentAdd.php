<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class CommentAdd
 *
 * Create a comment on a media object with the following rules:
 *  - The total length of the comment cannot exceed 300 characters.
 *  - The comment cannot contain more than 4 hashtags.
 *  - The comment cannot contain more than 1 URL.
 *  - The comment cannot consist of all capital letters.
 *
 * Required scope:  comments
 *                  public_content [required for media that does not belong to the owner of the access_token]
 *
 * @link https://www.instagram.com/developer/endpoints/comments/#post_media_comments
 * @package Manose\Instagram\Endpoint\Media
 */
class CommentAdd extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}/comments';

    protected $httpMethod = self::HTTP_POST;
    /**
     * @var array
     */
    private $errorList;

    /**
     * @param int $mediaId
     * @param string $comment
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId, $comment)
    {
        if (!is_numeric($mediaId)) {
            throw new EndpointException('Not valid Media ID');
        }

        if (!$this->isValidComment($comment)) {
            throw new EndpointException(implode('/', $this->errorList));
        }

        $this->endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
    }

    /**
     * @return array
     */
    public function getErrorList()
    {
        return $this->errorList;
    }

    /**
     * Check does comment is formatted according to instagram rules.
     *
     * @param $comment
     *
     * @return bool
     */
    public function isValidComment($comment)
    {
        $this->errorList = [];
        $maxCommentLength = 300;
        $maxHashTagAmount = 4;
        $maxUrlAmount = 1;

        if (mb_strlen($comment > $maxCommentLength)) {
            $this->errorList[] = 'The total length of the comment cannot exceed ' . $maxCommentLength . 'characters';
        }

        if (mb_substr_count($comment, '#') > $maxHashTagAmount) {
            $this->errorList[] = 'The comment cannot contain more than ' . $maxHashTagAmount . ' hashtags';
        }

        if (preg_match_all('#(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+#', $comment) > $maxUrlAmount) {
            $this->errorList[] = 'The comment cannot contain more than ' . $maxUrlAmount . ' URL';
        }

        if (true === ctype_upper($comment)) {
            $this->errorList[] = 'The comment cannot consist of all capital letters';
        }

        return empty($this->errorList);
    }
}
