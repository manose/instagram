<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class LikeDeleave
 *
 * Set a like on this media by the currently authenticated user.
 *
 * Required scope:  likes
 *                  public_content [required for media that does not belong to the owner of the access_token]
 *
 * @link https://www.instagram.com/developer/endpoints/likes/#post_likes
 *
 * @package Manose\Instagram\Endpoint\Media
 */
class LikeAdd extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/{media-id}/likes';

    protected $httpMethod = self::HTTP_POST;

    /**
     * @param int $mediaId
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($mediaId)
    {
        $this->endpointAction = str_replace('{media-id}', $mediaId, self::ACTION_ENDPOINT);
    }
}
