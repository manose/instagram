<?php
namespace Manose\Instagram\Endpoint\Media;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class InfoShortcode
 *
 * This endpoint returns the same response as GET /media/media-id.
 * A media object's shortcode can be found in its shortlink URL.
 * An example shortlink is http://instagram.com/p/tsxp1hhQTG/.
 * Its corresponding shortcode is tsxp1hhQTG.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/media/#get_media_by_shortcode
 * @package Manose\Instagram\Endpoint\Media
 */
class InfoShortcode extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/media/shortcode/{shortcode}';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param string $shortCode
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($shortCode)
    {
        if (!is_string($shortCode)) {
            throw new EndpointException('Wrong shortcode: ' . $shortCode);
        }

        $this->endpointAction = str_replace('{shortcode}', $shortCode, self::ACTION_ENDPOINT);
    }
}
