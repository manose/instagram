<?php
namespace Manose\Instagram\Endpoint;

use GuzzleHttp\ClientInterface;

/**
 * Class EndpointRequestRequestService
 *
 * Build and make requests to Instagram Endpoint API.
 *
 * @package Manose\Instagram\Endpoint
 */
class EndpointRequestService implements EndpointRequestServiceInterface
{
    const ENDPOINT_API_URL = 'https://api.instagram.com/v1/';
    /**
     * @var string
     */
    private $token;
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @param string          $token
     * @param ClientInterface $httpClient
     */
    public function __construct($token, ClientInterface $httpClient)
    {
        $this->token = $token;
        $this->httpClient = $httpClient;
    }

    /**
     * @inheritdoc
     */
    public function doEndpointRequest(EndpointInterface $endpoint)
    {
        $action = $endpoint->getEndpointAction();
        $httpMethod = $endpoint->getHttpMethod();
        $params = $endpoint->getEndpointParams();
        $uri = self::ENDPOINT_API_URL . $action;

        $params = $this->prepareParams($httpMethod, $params);
        $response = $this->httpClient->request($httpMethod, $uri, $params);

        return $response->getBody()->getContents();
    }

    /**
     * Add default fields to parameters and prepare them to send via Guzzle.
     *
     * @param string $httpMethod
     * @param array $params
     *
     * @return array
     */
    private function prepareParams($httpMethod, $params)
    {
        $encType = 'query';

        if (EndpointInterface::HTTP_POST === $httpMethod) {
            $encType = 'form_params';
        }

        $params['access_token'] = $this->token;

        return [
            $encType => $params,
        ];
    }
}
