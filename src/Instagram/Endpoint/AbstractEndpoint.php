<?php
namespace Manose\Instagram\Endpoint;

abstract class AbstractEndpoint implements EndpointInterface
{
    /**
     * Alias for self request
     */
    const MY_ACCOUNT_ALIAS = 'self';

    /**
     * @var string
     */
    protected $endpointAction;
    /**
     * Set method to send request to API.
     * Must be overwritten in child class.
     *
     * @var string
     */
    protected $httpMethod;
    /**
     * @var array
     */
    protected $endpointParams = [];

    /**
     * @inheritdoc
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @inheritdoc
     */
    public function getEndpointAction()
    {
        return $this->endpointAction;
    }

    /**
     * @inheritdoc
     */
    public function getEndpointParams()
    {
        return $this->endpointParams;
    }
}
