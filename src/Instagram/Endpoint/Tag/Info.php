<?php
namespace Manose\Instagram\Endpoint\Tag;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Info
 *
 * Get a list of recently tagged media.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/tags/#get_tags
 * @package Manose\Instagram\Endpoint\Tag
 */
class Info extends AbstractEndpoint
{
    /**
     * Action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = '/tags/{tag-name}';

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     * @param string $tagname
     *
     * @return void
     */
    public function prepareParams($tagname)
    {
        $this->endpointAction = str_replace('{tag-name}', $tagname, self::ACTION_ENDPOINT);
    }
}
