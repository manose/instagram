<?php
namespace Manose\Instagram\Endpoint\Tag;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Search
 *
 * Search for tags by name.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/tags/#get_tags_search
 * @package Manose\Instagram\Endpoint\Tag
 */
class Search extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/tags/search';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param string $search    A valid tag name without a leading #. (eg. snowy, nofilter)
     *
     * @return void
     */
    public function prepareParams($search)
    {
        $params['q'] = $search;

        $this->endpointParams = $params;
        $this->endpointAction = self::ACTION_ENDPOINT;
    }
}
