<?php
namespace Manose\Instagram\Endpoint\Tag;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Recent
 *
 * Get a list of recently tagged media.
 *
 * Required scope: public_content
 *
 * @link https://www.instagram.com/developer/endpoints/tags/#get_tags_media_recent
 * @package Manose\Instagram\Endpoint\Tag
 */
class Recent extends AbstractEndpoint
{
    /**
     * Action uri
     *
     * @var string
     */
    const ACTION_ENDPOINT = '/tags/{tag-name}/media/recent';

    /**
     * @var string
     */
    protected $httpMethod = self::HTTP_GET;

    /**
     * @param string $tagname
     * @param int $count [optional] Count of tagged media to return.
     * @param int $minTagId [optional] Return media before this min_tag_id.
     * @param int $maxTagId [optional] Return media after this max_tag_id.
     *
     * @return void
     */
    public function prepareParams($tagname, $count = null, $minTagId = null, $maxTagId = null)
    {
        $params['count'] = $count;
        $params['min_tag_id'] = $minTagId;
        $params['max_tag_id'] = $maxTagId;

        $this->endpointParams = $params;
        $this->endpointAction = str_replace('{tag-name}', $tagname, self::ACTION_ENDPOINT);
    }
}
