<?php
namespace Manose\Instagram\Endpoint;

interface EndpointInterface
{
    /**
     * Supported HTTP method GET
     */
    const HTTP_GET = 'GET';
    /**
     * Supported HTTP method POST
     */
    const HTTP_POST = 'POST';
    /**
     * Supported HTTP method DELETE
     */
    const HTTP_DELETE = 'DELETE';

    /**
     * Return one of REST method: GET, POST or DELETE
     *
     * @return string
     */
    public function getHttpMethod();

    /**
     * Get name of endpoint action
     *
     * @return string
     */
    public function getEndpointAction();

    /**
     * Get endpoint params.
     * For different endpoints params are different, therefore see inherited classes.
     *
     * @return array
     */
    public function getEndpointParams();
}
