<?php
namespace Manose\Instagram\Endpoint\Relationship;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class RequestedBy
 *
 * List the users who have requested this user's permission to follow.
 *
 * Required scope: follower_list
 *
 * @link https://www.instagram.com/developer/endpoints/relationships/#get_incoming_requests
 * @package Manose\Instagram\Endpoint\Relationship
 */
class RequestedBy extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = '/users/self/requested-by';

    protected $httpMethod = self::HTTP_GET;

    protected $endpointAction = self::ACTION_ENDPOINT;
}
