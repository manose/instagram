<?php
namespace Manose\Instagram\Endpoint\Relationship;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Relationship
 *
 * Get information about a relationship to another user.
 * Relationships are expressed using the following terms in the response:
 * outgoing_status: Your relationship to the user. Can be 'follows', 'requested', 'none'.
 * incoming_status: A user's relationship to you. Can be 'followed_by', 'requested_by', 'blocked_by_you', 'none'.
 *
 * Required scope: follower_list
 *
 * @link https://www.instagram.com/developer/endpoints/relationships/#get_relationship
 * @package Manose\Instagram\Endpoint\Relationship
 */
class Relationship extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = 'users/{user-id}/relationship';

    protected $httpMethod = self::HTTP_GET;

    /**
     * @param int $userId
     *
     * @return void
     */
    public function prepareParams($userId)
    {
        $this->endpointAction = str_replace('{user-id}', $userId, self::ACTION_ENDPOINT);
    }
}
