<?php
namespace Manose\Instagram\Endpoint\Relationship;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class FollowedBy
 *
 * Get the list of users this user is followed by.
 * Required scope: follower_list
 *
 * @link https://www.instagram.com/developer/endpoints/relationships/#get_users_followed_by
 * @package Manose\Instagram\Endpoint\Relationship
 */
class FollowedBy extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = 'users/self/followed-by';

    protected $httpMethod = self::HTTP_GET;

    protected $endpointAction = self::ACTION_ENDPOINT;
}
