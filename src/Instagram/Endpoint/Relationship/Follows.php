<?php
namespace Manose\Instagram\Endpoint\Relationship;

use Manose\Instagram\Endpoint\AbstractEndpoint;

/**
 * Class Follows
 *
 * Get the list of users this user follows.
 * Required scope: follower_list
 *
 * @link https://www.instagram.com/developer/endpoints/relationships/#get_users_follows
 * @package Manose\Instagram\Endpoint\Relationship
 */
class Follows extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = 'users/self/follows';

    protected $httpMethod = self::HTTP_GET;

    protected $endpointAction = self::ACTION_ENDPOINT;
}
