<?php
namespace Manose\Instagram\Endpoint\Relationship;

use Manose\Instagram\Endpoint\AbstractEndpoint;
use Manose\Instagram\Endpoint\EndpointException;

/**
 * Class Relationship
 *
 * Modify the relationship between the current user and the target user.
 * Relationships are expressed using the following terms in the response:
 * outgoing_status: Your relationship to the user. Can be 'follows', 'requested', 'none'.
 * incoming_status: A user's relationship to you. Can be 'followed_by', 'requested_by', 'blocked_by_you', 'none'.
 *
 * Required scope: relationships
 *
 * @link https://www.instagram.com/developer/endpoints/relationships/#post_relationship
 *
 * @package Manose\Instagram\Endpoint\Relationship
 */
class RelationshipModify extends AbstractEndpoint
{
    /**
     * @var string
     */
    const ACTION_ENDPOINT = 'users/{user-id}/relationship';

    const RELATION_ACTION_FOLLOW = 'follow';
    const RELATION_ACTION_UNFOLLOW = 'unfollow';
    const RELATION_ACTION_APPROVE = 'approve';
    const RELATION_ACTION_IGNORE = 'ignore';

    const RELATION_ACTION_LIST = [
        self::RELATION_ACTION_APPROVE,
        self::RELATION_ACTION_UNFOLLOW,
        self::RELATION_ACTION_FOLLOW,
        self::RELATION_ACTION_IGNORE,
    ];

    protected $httpMethod = self::HTTP_POST;

    /**
     * @param string $action    One of self::RELATION_ACTION_LIST
     * @param int $userId       ID of user to whom relation is changing
     *
     * @return void
     *
     * @throws EndpointException
     */
    public function prepareParams($action, $userId)
    {
        if (!in_array($action, self::RELATION_ACTION_LIST, true)) {
            throw new EndpointException('Action is not found:' . $action);
        }

        $this->endpointParams['action'] = $action;
        $this->endpointAction = str_replace('{user-id}', $userId, self::ACTION_ENDPOINT);
    }
}
